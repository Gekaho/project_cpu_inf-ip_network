#! /bin/bash

count=0

for host in {1..254}
do
   host_ip=192.168.1.$host
   result=`ping -w 1 $host_ip | grep "1 received"`
   if [ "$result" != "" ]
   then
	echo $host_ip
	let "count=count+1"
   fi
done

echo  Total -  $count 


#! /bin/bash 


# Function Proc: we used the following command three times: "cat /proc/cpuinfo". Every time we took the part we needed.
function Proc {
echo -e ..........."\033[32mPROC.INFO\033[0m"........
NUMBER_PROC=`cat /proc/cpuinfo | grep  -c "proc"`
  echo "The number of processors:  $NUMBER_PROC" 
MODEL_NAME=`cat /proc/cpuinfo | grep  "model name" | head -1`
  echo "$MODEL_NAME"
VENDOR_ID=`cat /proc/cpuinfo | grep "vendor_id" | head -1`
  echo "$VENDOR_ID"
FPU=`cat /proc/cpuinfo | grep "fpu" | head -1`
  echo "$FPU"
}


# Function HDD: we used the following command: "lsblk -rn" and we took the part we needed.
function HDD {
echo -e ..........."\033[36mHDD\033[0m"........
echo "`lsblk -rn | grep "sd" | cut -d" " -f1,4`"
}


# Function RAM: we used the following command three times: " free -h | grep "Mem"". Every time we took the part we needed.
function RAM {
 echo -e .............."\033[33mRAM\033[0m"...........
COMMAND_FREE="` free -h | grep "Mem"`"
echo Total memory : `echo $COMMAND_FREE | cut -d" " -f2`
echo Used memory : `echo $COMMAND_FREE | cut -d" " -f3` 
echo Free memory : `echo $COMMAND_FREE | cut -d" " -f4`
}


# Function System: 
function System {
 echo -e .........."\033[34mOPER.SYSTEM\033[0m".......
echo "`uname --all | cut -d" " -f1,3,4,12`"
}


# Function Internet: we used the following command: "ifconfig | grep "eth"". We look for this word: "eth". If the program finds this word, it prints "Yes", or else it prints "No". Then we look for word "wlan" this way.
function Internet {
 echo -e ..........."\033[35mETH.CONFIG\033[0m".........
ETH=`ifconfig | grep "eth"`
if [ -n "$ETH" ]
then 
   echo "eth           : YES"
else 
   echo "eth           : NO"
fi 
WLAN=`ifconfig | grep "wlan"`
if [ "$WLAN" != "" ]
then 
   echo "wlan          : YES"
else 
   echo "wlan          : NO"
fi 
}

# Function Display: every time we used the following command: "xwininfo -root". We created variable, which we will look for. Every time the variable must change.
function Display {
 echo -e .........."\033[31mDisplay\033[0m".......
for DISP in {"Width:","Height:","Depth:","Visual:"," Visual Class:","Colormap:","Bit Gravity State:"," Window Gravity State:","Backing Store State:","Save Under State:","Map State:","Override Redirect State:","Corners:"}
do
 echo `xwininfo -root |grep "$DISP"`
done
} 

# Function Help:
function Help {
echo "Arguments you have input are incorrect. If you want to input correct arguments you should follow these instructions:
if you want to get information about the processor you should input the argument this way: --proc
if you want to get information about hard disks you should input the argument this way: --hdd
if you want to get information about RAM you should input the argument this way: --ram
if you want to get information about the operting system you should input the argument this way: --system
if you want to get information about the internet you should input the argument this way: --internet
finaly if you want to get information about all of them you should input the argument this way: --all"
}

# First, if our command doesn't have arguments, the program will apply the function "Help".
if [ -z "$*" ]
then
Help
fi


# If our first argument is "--all" the program will apply all the functions except "Help". If "--all" is are second, third, etc. argument the program will apply only "Help" function. If all these functions are applied before the sixth argument, the program will apply the same function. If we input incorrect argument the program will apply only "Help" function.
for ARG in {$1,$2,$3,$4,$5,$6}
do
if [ $1 = "--all" ]
then
Proc
HDD
RAM
System
Internet
Display
elif [ $ARG = "--proc" ]
then
Proc
elif [ $ARG = "--hdd" ]
then
HDD
elif [ $ARG = "--ram" ]
then
RAM
elif [ $ARG = "--system" ]
then
System
elif [ $ARG = "--internet" ]
then
Internet
elif [ $ARG = "--display" ]
then
Display
else
reset
Help
fi
done




